﻿using System;
using System.Net;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Text;
using static System.Console;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("Введите веб-адрес и нажмите Enter:");
            string URI = ReadLine(); //"https://ru.stackoverflow.com";
            List<string> urls;

            string html = GetHTML(URI);

            if (html.StartsWith("Err"))
            {
                WriteLine($"Ошибка при обращении к сайту {URI}:");
                WriteLine(html.Substring(3));
            }
            else if (string.IsNullOrWhiteSpace(html))
            {
                WriteLine($"HTML данные не были загружены с сайта {URI}:");
            }
            else
            {
                urls = GetUriMatches(html);

                if (urls.Count > 0)
                {
                    int i = 1;
                    foreach (string url in urls)
                    {
                        WriteLine($"URI {i}: {url}");
                        i++;
                    }
                }
                else
                {
                    WriteLine("Ссылки не найдены...");
                }
            }

            ReadLine();
        }

        static string GetHTML(string uri)
        {
            try
            {
                string data = "";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream resStream = response.GetResponseStream())
                    {
                        using (StreamReader rdr = new StreamReader(resStream, Encoding.UTF8))
                        {
                            data = rdr.ReadToEnd();
                        }
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                return "Err" + ex.Message;
            }
        }

        static List<string> GetUriMatches(string input)
        {
            string pattern = @"(\w+\s?=?\s?)(?:['""]?(http\w?([^<>'""]{3,}))['""]?)";
            //  \w+             -- 1-n буквенно-цифровых символов (для поиска href/src etc.)
            //  \s?=?\s?        -- пробел, знак =, пробел - кады символ от 0 до 1 раз
            //  (?: ... )       -- невыделяеммая группа
            //  ['""]?          -- одинарная или двойная кавычка 0 или 1 раз (вторые кавычки для экранирования)
            //  http\w?         -- текст http и 0-1 буквенно-цифровой символ
            //  ([^<>'""]{3,})  -- группа любых символов, кроме <>'", повторяется минимум 3 раза

            List<string> result = new List<string>();

            Match m;
            try
            {
                m = Regex.Match(input, pattern,
                    RegexOptions.IgnoreCase | RegexOptions.Compiled,
                    TimeSpan.FromSeconds(1));

                int i = 1;
                while (m.Success)
                {
                    result.Add(m.Groups[2].ToString());
                    m = m.NextMatch();
                    i++;
                }
            }
            catch (RegexMatchTimeoutException)
            {
                WriteLine("Operation timed out.");
            }

            return result;
        }
    }
}
